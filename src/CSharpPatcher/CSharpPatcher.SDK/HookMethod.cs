using System;

namespace CSharpPatcher.SDK
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor)]
    public class HookMethod : Attribute
    {
        public string AssemblyPath { get; private set; }
        public string Namespace { get; private set; }
        public string Classname { get; private set; }
        public string Methodname { get; private set; }
        public string Args { get; private set; }
        public ETypeInject TypeInject { get; private set; }
        public int InjectIndex { get; private set; }
        public int MethodNumber { get; private set; }

        public HookMethod(string assemblyPath, string _namespace, string _classname, string _methodname, string _args = "", ETypeInject _typeInject = ETypeInject.InjectMethod, int _injectIndex = 0, int _methodNumber = 0)
        {
            this.AssemblyPath = assemblyPath;
            this.Namespace = _namespace;
            this.Classname = _classname;
            this.Methodname = _methodname;
            this.Args = _args;
            this.TypeInject = _typeInject;
            this.InjectIndex = _injectIndex;
            this.MethodNumber = _methodNumber;
        }
        
        public enum ETypeInject
        {
            InjectMethod,
            RewriteMethod
        }
    }
}