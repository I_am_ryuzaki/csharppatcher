using System;

namespace CSharpPatcher.SDK
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor)]
    public class DebugMethod : Attribute
    {
        public string AssemblyPath { get; private set; }
        public string Namespace { get; private set; }
        public string Classname { get; private set; }
        public string Methodname { get; private set; }

        public DebugMethod(string assemblyPath, string _namespace, string _classname, string _methodname)
        {
            this.AssemblyPath = assemblyPath;
            this.Namespace = _namespace;
            this.Classname = _classname;
            this.Methodname = _methodname;
        }
    }
}