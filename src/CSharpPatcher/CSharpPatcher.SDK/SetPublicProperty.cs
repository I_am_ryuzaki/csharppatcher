using System;

namespace CSharpPatcher.SDK
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor)]
    public class SetPublicProperty : Attribute
    {
        public string AssemblyPath { get; private set; }
        public string Namespace { get; private set; }
        public string Classname { get; private set; }
        public string Propertyname { get; private set; }

        public SetPublicProperty(string assemblyPath, string _namespace, string _classname, string _propertyname)
        {
            this.AssemblyPath = assemblyPath;
            this.Namespace = _namespace;
            this.Classname = _classname;
            this.Propertyname = _propertyname;
        }
    }
}