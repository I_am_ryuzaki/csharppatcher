using System;

namespace CSharpPatcher.SDK
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor)]
    public class SetPublicMethod : Attribute
    {
        public string AssemblyPath { get; private set; }
        public string Namespace { get; private set; }
        public string Classname { get; private set; }
        public string Methodname { get; private set; }
        public int MethodNumber { get; private set; }

        public SetPublicMethod(string assemblyPath, string _namespace, string _classname, string _methodname, int _methodNumber = 0)
        {
            this.AssemblyPath = assemblyPath;
            this.Namespace = _namespace;
            this.Classname = _classname;
            this.Methodname = _methodname;
            this.MethodNumber = _methodNumber;
        }
    }
}