using System;

namespace CSharpPatcher.SDK
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor)]
    public class SetPublicField : Attribute
    {
        public string AssemblyPath { get; private set; }
        public string Namespace { get; private set; }
        public string Classname { get; private set; }
        public string Fieldname { get; private set; }

        public SetPublicField(string assemblyPath, string _namespace, string _classname, string _fieldname)
        {
            this.AssemblyPath = assemblyPath;
            this.Namespace = _namespace;
            this.Classname = _classname;
            this.Fieldname = _fieldname;
        }
    }
}