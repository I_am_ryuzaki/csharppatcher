using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using ILRepacking;
using Mono.Cecil;

namespace CSharpPatcher
{
    public class Bootstrap
    {
        public static String TargetPath { get; private set; }
        public static String TargetDirectory { get; private set; }
        public static AssemblyDefinition TargetDefinition { get; private set; }
        
        static void Main(string[] args)
        {
            Console.WriteLine("Application has been started!\n");
            if (args.Length == 0)
                FatalError("Not found path from params!");
            
            TargetPath = args[0];
            TargetDirectory = new FileInfo(TargetPath).DirectoryName;
            
            if (File.Exists(TargetDirectory + "/CSharpPatcher.SDK.dll") == false)
                File.Copy(AppDomain.CurrentDomain.BaseDirectory + "/CSharpPatcher.SDK.dll", TargetDirectory + "/CSharpPatcher.SDK.dll");

            string backupPath = TargetPath + ".backup." + DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss");
            File.Copy(TargetPath, backupPath);
            
            Console.WriteLine("Application pre initialization has been success!\n");

            EndMerge();

            TargetDefinition = TargetPath.LoadAssembly();
            if (TargetDefinition == null)
                FatalError("Target assembly not loaded!");
            
            ModuleInjector.LoadModules();
            
            Console.WriteLine("\n\n### Finish!");
            Console.ReadLine();
        }

        public static void EndMerge()
        {
            File.Move(TargetPath, TargetPath + ".premerge");
            try
            {
                List<string> inputFiles = new List<string>();
                inputFiles.Add(TargetPath + ".premerge");
                inputFiles.Add(new FileInfo(AppDomain.CurrentDomain.BaseDirectory +  "/CSharpPatcher.SDK.dll").FullName);
                
                List<string> searchDir = new List<string>()
                {
                    AppDomain.CurrentDomain.BaseDirectory,
                    TargetPath
                };
                
                foreach (var fileInfo in Ext.GetModulesList())
                    if (fileInfo.Name != "CSharpPatcher.SDK.dll")
                        inputFiles.Add(fileInfo.FullName);

                ILRepack ilRepack = new ILRepack(new RepackOptions()
                {
                    InputAssemblies = inputFiles.ToArray(),
                    TargetKind = (TargetPath.Substring(TargetPath.Length-3) == "dll" ? ILRepack.Kind.Dll : ILRepack.Kind.Exe),
                    OutputFile = TargetPath,
                    CopyAttributes = true,
                    TargetPlatformVersion = "v4",
                    SearchDirectories = searchDir.AsEnumerable()
                });
                ilRepack.Repack();
                File.Delete(TargetPath + ".premerge");
            }
            catch (Exception ex)
            {
                File.Move(TargetPath + ".premerge", TargetPath);
                FatalError("Exception to merge: " + ex);
            }
        }


        public static void FatalError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ReadLine();
            System.Environment.FailFast(message);
        }
    }
}