using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Collections.Generic;

namespace CSharpPatcher
{
    public static class Ext
    {
        private static Dictionary<MethodDefinition, ILProcessor> listIlProcessors = new Dictionary<MethodDefinition, ILProcessor>();
        
        public static ILProcessor GetILProcessor(this MethodDefinition method)
        {
            if (listIlProcessors.TryGetValue(method, out ILProcessor processor))
                return processor;
            listIlProcessors[method] = method.Body.GetILProcessor();
            return listIlProcessors[method];
        }
        
        public static MethodDefinition GetMethod(this Collection<MethodDefinition> methods, string name)
        {
            return methods.FirstOrDefault(m => m.Name == name);
        }
        public static MethodDefinition GetMethod(this TypeDefinition type, string name)
        {
            return type.Methods.GetMethod(name);
        }
        
        public static TypeDefinition FindType(this AssemblyDefinition assemblyDefinition, string _namespace, string _classname)
        {
            string[] _classes = _classname.Split(new char[] {'.'}, StringSplitOptions.RemoveEmptyEntries);
            if (_classes.Length == 0)
                return null;
            TypeDefinition baseType = assemblyDefinition.MainModule.GetType(_namespace, _classes[0]);
            if (baseType == null)
                return null;
            if (_classes.Length > 1)
            {
                for (var i = 1; i < _classes.Length; i++)
                {
                    baseType = baseType.NestedTypes.FirstOrDefault(type => type.Name == _classes[i]);
                    if (baseType == null)
                        return null;
                }
            }
            return baseType;
        }

        public static MethodDefinition FindMethod(this AssemblyDefinition assemblyDefinition, string _namespace, string _classname, string _methodName, int indexMethod)
        {
            TypeDefinition baseType = assemblyDefinition.FindType(_namespace, _classname);
            if (baseType != null)
            {
                MethodDefinition[] methods = baseType.Methods.Where(method => method.Name == _methodName).ToArray();
                if (methods.Length - 1 >= indexMethod)
                    return methods[indexMethod];
            }
            return null;
        }

        public static AssemblyDefinition LoadAssembly(this string path)
        {
            try
            {
                MemoryStream stream = new MemoryStream(File.ReadAllBytes(path));
                return AssemblyDefinition.ReadAssembly(stream);
            }
            catch (Exception ex)
            {
                Bootstrap.FatalError("Exception LoadAssembly: " + ex.Message);
            }
            return null;
        }

        public static FileInfo[] GetModulesList()
        {
            try
            {
                if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Modules/") == false)
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/Modules/");
                DirectoryInfo dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "/Modules/");
                return dir.GetFiles("*.dll");
            }
            catch
            {
                
            }
            return new FileInfo[0];
        }

        public static bool IsHookedMethod(this MethodDefinition method)
        {
            ILProcessor processor = method.GetILProcessor();
            if (processor.Body.Instructions.Count >= 3)
            {
                if (processor.Body.Instructions[0].OpCode == OpCodes.Nop && processor.Body.Instructions[1].OpCode == OpCodes.Nop && processor.Body.Instructions[2].OpCode == OpCodes.Nop)
                    return true;
            }
            return false;
        }

        public static void SetHookedMethod(this MethodDefinition method)
        {
            ILProcessor processor = method.GetILProcessor();
            if (processor.Body.Instructions.Count >= 1)
            {
                Instruction firstInstruction = processor.Body.Instructions[0];
                processor.InsertBefore(firstInstruction, Instruction.Create(OpCodes.Nop));
                processor.InsertBefore(firstInstruction, Instruction.Create(OpCodes.Nop));
                processor.InsertBefore(firstInstruction, Instruction.Create(OpCodes.Nop));
            }
        }
        
    }
}