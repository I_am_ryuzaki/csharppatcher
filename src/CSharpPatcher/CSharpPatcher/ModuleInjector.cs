using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using CSharpPatcher.SDK;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace CSharpPatcher
{
    public class ModuleInjector
    {
        private static Dictionary<string, AssemblyDefinition> ListAssemblyOpened = new Dictionary<string, AssemblyDefinition>();
        public static void LoadModules()
        {
            for (var i = 0; i < Bootstrap.TargetDefinition.MainModule.Types.Count; i++)
            {
                TypeDefinition type = Bootstrap.TargetDefinition.MainModule.Types[i];
                for (var j = 0; j < type.Methods?.Count; j++)
                {
                    MethodDefinition method = type.Methods[j];
                    for (var l = 0; l < method.CustomAttributes?.Count; l++)
                    {
                        try
                        {
                            CustomAttribute attr = method.CustomAttributes[l];
                            switch (attr.AttributeType.FullName)
                            {
                                case "CSharpPatcher.SDK.HookMethod":
                                    Console.WriteLine($"Detected [HookMethod] from [{method.Name}] method!");
                                    UseHookMethod(method, attr);
                                    break;
                                case "CSharpPatcher.SDK.SetPublicField":
                                    Console.WriteLine($"Detected [SetPublicField] from [{method.Name}] method!");
                                    SetPublicField(method, attr);
                                    break;
                                case "CSharpPatcher.SDK.SetPublicProperty":
                                    Console.WriteLine($"Detected [SetPublicProperty] from [{method.Name}] method!");
                                    SetPublicProperty(method, attr);
                                    break;
                                case "CSharpPatcher.SDK.SetPublicMethod":
                                    Console.WriteLine($"Detected [SetPublicMethod] from [{method.Name}] method!");
                                    SetPublicMethod(method, attr);
                                    break;
                                case "CSharpPatcher.SDK.DebugMethod":
                                    Console.WriteLine($"Detected [DebugMethod] from [{method.Name}] method!");
                                    UseDebugMethod(method, attr);
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception to scan method [{method.Name}]: " + ex);
                        }
                    }
                }
            }

            foreach (var assemblyPair in ListAssemblyOpened)
            {
                try
                {
                    string backupPath = Bootstrap.TargetDirectory + "/" + assemblyPair.Key + ".m_backup." + DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss");
                    File.Copy(Bootstrap.TargetDirectory + "/" + assemblyPair.Key, backupPath);
                    assemblyPair.Value.Write(Bootstrap.TargetDirectory + "/" + assemblyPair.Key);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception to save assembly [{assemblyPair.Key}]: " + ex);
                }
            }
        }

        public static void UseDebugMethod(MethodDefinition method, CustomAttribute attr)
        {
            string _assembly = (string) attr.ConstructorArguments[0].Value;
            if (LoadAssembly(_assembly))
            {
                string _namespace = (string) attr.ConstructorArguments[0].Value;
                string _classname = (string) attr.ConstructorArguments[1].Value;
                string _methodname = (string) attr.ConstructorArguments[2].Value;

                TypeDefinition baseType = ListAssemblyOpened[_assembly].FindType(_namespace, _classname);
                if (baseType != null)
                {
                    MethodDefinition[] methods = baseType.Methods.Where(m => m.Name == _methodname).ToArray();
                    for (var i = 0; i < methods.Length; i++)
                    {
                        Console.Write($"[{i}] => " + methods[i].Name + "(");
                        for (var j = 0; j < methods[i].Parameters.Count; j++)
                        {
                            Console.Write((j == 0 ? "" : ", ") + methods[j].Parameters[j].ParameterType.Name);
                        }

                        Console.WriteLine(")");
                    }
                }
                else
                    Console.WriteLine($"Class [{_namespace}.{_classname}] not found!");
            }
        }

        public static void UseHookMethod(MethodDefinition method, CustomAttribute attr)
        {
            string _assembly = (string) attr.ConstructorArguments[0].Value;
            if (LoadAssembly(_assembly))
            {
                string _namespace = (string) attr.ConstructorArguments[1].Value;
                string _classname = (string) attr.ConstructorArguments[2].Value;
                string _methodname = (string) attr.ConstructorArguments[3].Value;
                string _args = (string) attr.ConstructorArguments[4].Value;
                HookMethod.ETypeInject _typeInject = (HookMethod.ETypeInject) attr.ConstructorArguments[5].Value;
                int _injectIndex = (int) attr.ConstructorArguments[6].Value;
                int _methodNumber = (int) attr.ConstructorArguments[7].Value;
                try
                {
                    MethodDefinition method_target = ListAssemblyOpened[_assembly].FindMethod(_namespace, _classname, _methodname, _methodNumber);
                    if (method_target != null)
                    {
                        if (method_target.IsHookedMethod() == false)
                        {
                            ILProcessor ilProcessor = method_target.GetILProcessor();
                            if (ilProcessor.Body.Instructions.Count >= _injectIndex + 1)
                            {
                                Instruction firstInstruction = ilProcessor.Body.Instructions[_injectIndex];
                                _args = _args.Replace(" ", "");
                                string[] args_array = _args.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);
                                for (var i = 0; i < args_array.Length; i++)
                                {
                                    string arg = args_array[i];
                                    if (arg == "self")
                                        ilProcessor.InsertBefore(firstInstruction, Instruction.Create(OpCodes.Ldloc_0));
                                    else if (arg.StartsWith("arg"))
                                    {
                                        if (int.TryParse(arg.Substring(3), out int num))
                                            if (method_target.Parameters.Count >= num)
                                                ilProcessor.InsertBefore(firstInstruction, Instruction.Create(OpCodes.Ldarg_S, method_target.Parameters[num]));
                                    }
                                    else if (arg.StartsWith("loc"))
                                    {
                                        if (int.TryParse(arg.Substring(3), out int num))
                                            if (ilProcessor.Body.Variables.Count >= num)
                                                ilProcessor.InsertBefore(firstInstruction, Instruction.Create(OpCodes.Ldloc_S, ilProcessor.Body.Variables[num]));
                                    }
                                }

                                ilProcessor.InsertBefore(firstInstruction, Instruction.Create(OpCodes.Call, method_target.Module.ImportReference(method)));
                                if (_typeInject == HookMethod.ETypeInject.RewriteMethod)
                                {
                                    ilProcessor.InsertBefore(firstInstruction, Instruction.Create(OpCodes.Ret));
                                    ilProcessor.Body.ExceptionHandlers.Clear();
                                }

                                method_target.SetHookedMethod();
                            }
                            else
                                Console.WriteLine($"Method [{_namespace}.{_classname}.{_methodname}] is not have index {_injectIndex}!");
                        }
                        else
                            Console.WriteLine($"Method [{_namespace}.{_classname}.{_methodname}] is already hooked!");
                    }
                    else
                        Console.WriteLine($"Method [{_namespace}.{_classname}.{_methodname}] not found!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception to [UseHookMethod:{method.Name}:{_methodname}]: " + ex.Message);
                }
            }
        }

        public static void SetPublicField(MethodDefinition method, CustomAttribute attr)
        {
            string _assembly = (string) attr.ConstructorArguments[0].Value;
            if (LoadAssembly(_assembly))
            {
                string _namespace = (string) attr.ConstructorArguments[1].Value;
                string _classname = (string) attr.ConstructorArguments[2].Value;
                string _fieldname = (string) attr.ConstructorArguments[3].Value;
                try
                {
                    TypeDefinition type_target = ListAssemblyOpened[_assembly].FindType(_namespace, _classname);
                    if (type_target != null)
                    {
                        FieldDefinition _fieldDefinition = type_target.Fields.FirstOrDefault(f => f.Name == _fieldname);
                        if (_fieldDefinition != null)
                        {
                            _fieldDefinition.IsPrivate = false;
                            _fieldDefinition.IsPublic = true;
                        }
                        else
                            Console.WriteLine($"Field [{_namespace}.{_classname}.{_fieldDefinition}] not found!");
                    }
                    else
                        Console.WriteLine($"Class [{_namespace}.{_classname}] not found!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception to [SetPublicField:{method.Name}:{_fieldname}]: " + ex.Message);
                }
            }
        }

        public static void SetPublicProperty(MethodDefinition method, CustomAttribute attr)
        {
            string _assembly = (string) attr.ConstructorArguments[1].Value;
            if (LoadAssembly(_assembly))
            {
                string _namespace = (string) attr.ConstructorArguments[2].Value;
                string _classname = (string) attr.ConstructorArguments[3].Value;
                string _propertyname = (string) attr.ConstructorArguments[4].Value;
                try
                {
                    TypeDefinition type_target = ListAssemblyOpened[_assembly].FindType(_namespace, _classname);
                    if (type_target != null)
                    {
                        PropertyDefinition _properyDefinition = type_target.Properties.FirstOrDefault(f => f.Name == _propertyname);
                        if (_properyDefinition != null)
                        {
                            if (_properyDefinition.GetMethod != null)
                            {
                                _properyDefinition.GetMethod.IsPrivate = false;
                                _properyDefinition.GetMethod.IsPublic = true;
                            }

                            if (_properyDefinition.SetMethod != null)
                            {
                                _properyDefinition.SetMethod.IsPrivate = false;
                                _properyDefinition.SetMethod.IsPublic = true;
                            }
                        }
                        else
                            Console.WriteLine($"Property [{_namespace}.{_classname}.{_propertyname}] not found!");
                    }
                    else
                        Console.WriteLine($"Class [{_namespace}.{_classname}] not found!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception to [SetPublicProperty:{method.Name}:{_propertyname}]: " + ex.Message);
                }
            }
        }

        public static void SetPublicMethod(MethodDefinition method, CustomAttribute attr)
        {
            string _assembly = (string) attr.ConstructorArguments[0].Value;
            if (LoadAssembly(_assembly))
            {
                string _namespace = (string) attr.ConstructorArguments[1].Value;
                string _classname = (string) attr.ConstructorArguments[2].Value;
                string _methodname = (string) attr.ConstructorArguments[3].Value;
                int _methodNumber = (int) attr.ConstructorArguments[4].Value;
                try
                {
                    MethodDefinition method_target = ListAssemblyOpened[_assembly].FindMethod(_namespace, _classname, _methodname, _methodNumber);
                    if (method_target != null)
                    {
                        method_target.IsPrivate = false;
                        method_target.IsPublic = true;
                    }
                    else
                        Console.WriteLine($"Method [{_namespace}.{_classname}.{_methodname}] not found!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception to [SetPublicMethod:{method.Name}:{_methodname}]: " + ex.Message);
                }
            }
        }

        private static bool LoadAssembly(string assembly)
        {
            if (ListAssemblyOpened.ContainsKey(assembly))
                return true;
            if (File.Exists(Bootstrap.TargetDirectory + "/" + assembly) == false)
            {
                Console.WriteLine($"ERROR: Not found assembly [{assembly}]");
                return false;
            }

            try
            {
                ListAssemblyOpened[assembly] = (Bootstrap.TargetDirectory + "/" + assembly).LoadAssembly();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception to load assembly [{assembly}]: " + ex.Message);
            }
            return false;
        }
    }
}