using CSharpPatcher.SDK;

namespace NoSteam_RustClient
{
    public class NoSteam
    {
        [HookMethod("Assembly-CSharp.dll", "Rust", "Defines", ".cctor", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SetAppIDFromRust()
        {
            Rust.Defines.appID = 480;
        }
    }
}